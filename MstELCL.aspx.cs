﻿using System;
using System.Configuration;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Altius.BusinessAccessLayer.BALDataAccess;
using Payroll;
using System.Data.SqlClient;

public partial class MstELCL : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    static string CmpName;
    static string Cmpaddress;
    string SessionUserType;
    string SessionEpay;
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;
    string SSQL = "";
    string SessionPayroll;
    bool ErrFlg = false;
    string ss = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        SessionEpay = Session["SessionEpay"].ToString();
       
        con = new SqlConnection(constr);
        if (!IsPostBack)
        {
           
            int currentYear = Utility.GetCurrentYearOnly;
            for (int i = 0; i < 10; i++)
            {
                ddlFinance.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString(), currentYear.ToString()));
                //txtBonusYear.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString(), currentYear.ToString()));

                currentYear = currentYear - 1;
            }
           
        }
        Load_Data();
    }

    private void Load_Data()
    {
        SSQL = "";
        SSQL = "Select * from [" + SessionEpay + "]..MstELCL where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' order by Year,DaysFrom,DaysTo desc";
        Repeater1.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater1.DataBind();

    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (ddlCategory.SelectedValue == "0")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the Category')", true);
        }
        if (ddlEmployeeType.SelectedValue == "-Select-")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the Employee Type')", true);
        }
        if (txtDaysFrom.Text == "")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the Days From')", true);
        }
        if (txtDaysTo.Text == "")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the Days To')", true);
        }
        if (txtELDays.Text == "")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter EL Days')", true);
        }
        if (txtCLDays.Text == "")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter EL Days')", true);
        }
        if (!ErrFlg)
        {
            if (btnSave.Text == "Save")
            {
                SSQL = "";
                SSQL = "Select * from [" + SessionEpay + "]..MstELCL where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and CateCode='" + ddlCategory.SelectedValue + "' and EmpTypeCode='" + ddlEmployeeType.SelectedValue + "'";
                SSQL = SSQL + " and Year='" + ddlFinance.SelectedValue + "' and DaysFrom>='" + txtDaysFrom.Text + "' and '" + txtDaysFrom.Text + "'<=DaysTo";
                DataTable dt_check = new DataTable();
                dt_check = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt_check != null && dt_check.Rows.Count > 0)
                {
                    ErrFlg = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the Days From After last Days To')", true);
                }
            }
            if (!ErrFlg)

            {
                SSQL = "";
                SSQL = "delete from [" + SessionEpay + "]..MstELCL where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and CateCode='" + ddlCategory.SelectedValue + "' and EmpTypeCode='" + ddlEmployeeType.SelectedValue + "'";
                SSQL = SSQL + " and Year='" + ddlFinance.SelectedValue + "' and DaysFrom>='" + txtDaysFrom.Text + "' and DaysTo <= '" + txtDaysTo.Text + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);

                SSQL = "";
                SSQL = "Insert into [" + SessionEpay + "]..MstELCL(Ccode,Lcode,CateCode,CateName,EmpTypeCode,EmpTypeName,Year,DaysFrom,DaysTo,ELDays,CLDays,UserID)";
                SSQL = SSQL + " values('" + SessionCcode + "','" + SessionLcode + "','" + ddlCategory.SelectedValue + "','" + ddlCategory.SelectedItem.Text + "'";
                SSQL = SSQL + ", '" + ddlEmployeeType.SelectedValue + "','" + ddlEmployeeType.SelectedItem.Text + "','" + ddlFinance.SelectedValue + "','" + txtDaysFrom.Text + "','" + txtDaysTo.Text + "','" + txtELDays.Text + "','" + txtCLDays.Text + "','" + ss + "')";
                objdata.RptEmployeeMultipleDetails(SSQL);

                if (btnSave.Text == "Save")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('EL&CL Days Saved Successfully')", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('EL&CL Days Updated Successfully')", true);
                }

                btnCancel_Click(sender, e);
                
            }
        }
        Load_Data();
    }
    
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        ddlCategory.ClearSelection();
        ddlEmployeeType.ClearSelection();
        txtDaysFrom.Text = "";
        txtDaysTo.Text = "";
        txtELDays.Text = "";
        txtCLDays.Text = "";
        ddlFinance.ClearSelection();
        btnSave.Text = "Save";
    }

    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_EmpType(ddlCategory.SelectedValue);
    }

    private void Load_EmpType(string selectedValue)
    {
        SSQL = "";
        SSQL = "Select EmpType,EmpTypeCd from MstEmployeeType where EmpCategory='" + selectedValue + "'";
        ddlEmployeeType.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlEmployeeType.DataTextField = "EmpType";
        ddlEmployeeType.DataValueField = "EmpTypeCd";
        ddlEmployeeType.DataBind();
        ddlEmployeeType.Items.Insert(0, new ListItem("-Select-","-Select-"));

    }

    protected void btnEditEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        string EmpTypeCode = "";
        string CateCode = "";
        string DaysFrom = "0";
        string DaysTo = "0";

        string[] Array = e.CommandName.Split(',');

        CateCode = Array[0].ToString();
        EmpTypeCode = Array[1].ToString();

        string [] Array2 = e.CommandArgument.ToString().Split(',');

        DaysFrom = Array2[0];
        DaysTo = Array2[1];

        DataTable dt = new DataTable();
        SSQL = "";
        SSQL = "Select * from [" + SessionEpay + "]..MstELCL where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
        SSQL = SSQL + " and DaysFrom='" + DaysFrom + "' and DaysTo='" + DaysTo + "'";
        SSQL = SSQL + " and CateCode='" + CateCode + "' and EmpTypeCode='" + EmpTypeCode + "'";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt!=null&& dt.Rows.Count > 0)
        {
            ddlCategory.SelectedValue = dt.Rows[0]["CateCode"].ToString();
            ddlCategory_SelectedIndexChanged(sender, e);
            ddlEmployeeType.SelectedValue = dt.Rows[0]["EmpTypeCode"].ToString();
            ddlFinance.SelectedValue = dt.Rows[0]["Year"].ToString();
            txtDaysFrom.Text = dt.Rows[0]["DaysFrom"].ToString();
            txtDaysTo.Text = dt.Rows[0]["DaysTo"].ToString();
            txtELDays.Text = dt.Rows[0]["ELDays"].ToString();
            txtCLDays.Text = dt.Rows[0]["CLDays"].ToString();
            btnSave.Text = "Update";
        }
        Load_Data();
        
    }

    protected void btnDeleteEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        string EmpTypeCode = "";
        string CateCode = "";
        string DaysFrom = "";
        string DaysTo = "";

        string [] Array = e.CommandName.Split(',');

        CateCode = Array[0].ToString();
        EmpTypeCode = Array[1].ToString();

        Array = e.CommandArgument.ToString().Split(',');

        DaysFrom = Array[0];
        DaysTo = Array[1];

        SSQL = "";
        SSQL = "Delete from [" + SessionEpay + "]..MstELCL where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
        SSQL = SSQL + " and DaysFrom='" + DaysFrom + "' and DaysTo='" + DaysTo + "'";
        SSQL = SSQL + " and CateCode='" + CateCode + "' and EmpTypeCode='" + EmpTypeCode + "'";
        objdata.RptEmployeeMultipleDetails(SSQL);
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('EL&CL Days Deleted Successfully')", true);
        Load_Data();
        
    }
}