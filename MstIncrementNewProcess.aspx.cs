﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using Payroll;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Globalization;

public partial class MstIncrementNew : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string SessionEpay;
    string SSQL = "";
    string SessionPayroll;
    string CmpName;
    string Cmpaddress;

    bool ErrFlg = false;

    System.Web.UI.WebControls.DataGrid gv =
                          new System.Web.UI.WebControls.DataGrid();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        SessionEpay = Session["SessionEpay"].ToString();

        if (!IsPostBack)
        {
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                //ddlFinYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //currentYear = currentYear - 1;
            }
            load_wages();
        }
        Load_OLD_Data();
    }

    private void load_wages()
    {
        DataTable dt = new DataTable();

        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlWagesType.Items.Clear();
        query = "Select *from MstEmployeeType";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlWagesType.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpTypeCd"] = "0";
        dr["EmpType"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlWagesType.DataTextField = "EmpType";
        ddlWagesType.DataValueField = "EmpTypeCd";
        ddlWagesType.DataBind();
    }

    private void Load_OLD_Data()
    {
        SSQL = "";
        SSQL = "Select * from [" + SessionEpay + "]..MstDayIncProcess where Compcode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        Repeater1.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater1.DataBind();
    }

    protected void ddlFinYear_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (txtFromDate.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Enter From Date');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Month", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlg = true;
            return;
        }
        if (txtToDate.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Enter To Date');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Month", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlg = true;
            return;
        }
        if (ddlWagesType.SelectedItem.Text == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Choose Wages');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Month", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlg = true;
            return;

        }

        if (!ErrFlg)
        {
            if (btnSave.Text == "Process")
            {
                SSQL = "";
                SSQL = "Select * from [" + SessionEpay + "]..MstDayIncProcess where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and WagesCode='" + ddlWagesType.SelectedValue + "'";
                SSQL = SSQL + " And FromDate='" + txtFromDate.Text + "' And ToDate='" + txtToDate.Text + "'";
                DataTable dt = new DataTable();
                dt = objdata.RptEmployeeMultipleDetails(SSQL);

                if (dt.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Already increment Process Present in this dates Please try Another dates');", true);
                    //System.Windows.Forms.MessageBox.Show("Select the Month", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlg = true;
                    return;
                }
            }
            if (!ErrFlg)
            {
                SSQL = "";
                SSQL = "Delete from [" + SessionEpay + "]..MstDayIncProcess where Compcode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and WagesCode='" + ddlWagesType.SelectedValue + "'";
                SSQL = SSQL + " And FromDate='" + txtFromDate.Text + "' And ToDate='" + txtToDate.Text + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);

                SSQL = "";
                SSQL = "Insert into [" + SessionEpay + "]..MstDayIncProcess (CompCode,LocCode,FromDate,ToDate,Wages,WagesCode,Status,IncrementDate) ";
                SSQL = SSQL + "Values('" + SessionCcode + "','" + SessionLcode + "','" + txtFromDate.Text + "','" + txtToDate.Text + "','" + ddlWagesType.SelectedItem.Text + "','" + ddlWagesType.SelectedValue + "','2','" + txtIncDate.Text + "')";
                objdata.RptEmployeeMultipleDetails(SSQL);

                SalaryIncrement();

                if (btnSave.Text == "Process")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Increment Process Details Saved Successfully');", true);

                }
                if (btnSave.Text == "Update")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Increment Process Details Updated Successfully');", true);
                    //btnCancel_Click(sender, e);
                }
                btnCancel_Click(sender, e);
            }
        }
        Load_OLD_Data();
    }


    protected void btnCancel_Click(object sender, EventArgs e)
    {
        txtFromDate.Text = "";
        txtToDate.Text = "";
        txtIncDate.Text = "";
        btnSave.Text = "Process";
        ddlWagesType.ClearSelection();
    }

    protected void btnEditEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "Select * from [" + SessionEpay + "]..MstDayIncProcess where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' WagesCode='" + e.CommandName + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count > 0)
        {
            txtFromDate.Text = dt.Rows[0]["FromDate"].ToString();
            txtToDate.Text = dt.Rows[0]["ToDate"].ToString();
            ddlWagesType.SelectedValue = dt.Rows[0]["WagesCode"].ToString();
            txtIncDate.Text = dt.Rows[0]["IncrementDate"].ToString();

            btnSave.Text = "Update";
        }
        Load_OLD_Data();
    }

    protected void btnDeleteEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "Select * from [" + SessionEpay + "]..MstDayIncProcess where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and FromDate='" + e.CommandArgument.ToString().Split(',').First() + "'";
        SSQL = SSQL + " and ToDate='" + e.CommandArgument.ToString().Split(',').Last() + "' and WagesCode='" + e.CommandName.ToString() + "' and Status='1'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count > 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Increment Process Already Approved');", true);
        }
        else
        {
            SSQL = "";
            SSQL = "Delete from [" + SessionEpay + "]..MstDayIncProcess where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and FromDate='" + e.CommandArgument.ToString().Split(',').First() + "'";
            SSQL = SSQL + " and ToDate='" + e.CommandArgument.ToString().Split(',').Last() + "' and WagesCode='" + e.CommandName.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "";
            SSQL = "Delete from [" + SessionEpay + "]..EmpDaysIncrement where cCode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and FromDate='" + e.CommandArgument.ToString().Split(',').First() + "'";
            SSQL = SSQL + " and ToDate='" + e.CommandArgument.ToString().Split(',').Last() + "' and WagesCode='" + e.CommandName.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);

            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Increment Process Details Deleted Successfully');", true);
        }
        Load_OLD_Data();
    }
    protected void btnApproveEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "Select * from [" + SessionEpay + "]..MstDayIncProcess where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and FromDate='" + e.CommandArgument.ToString().Split(',').First() + "'";
        SSQL = SSQL + " and ToDate='" + e.CommandArgument.ToString().Split(',').Last() + "' and WagesCode='" + e.CommandName.ToString() + "' and Status='1'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt.Rows.Count > 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Increment Process Already Approved');", true);
        }
        else
        {
            SSQL = "";
            SSQL = "Update [" + SessionEpay + "]..MstDayIncProcess set Status='1' where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and FromDate='" + e.CommandArgument.ToString().Split(',').First() + "'";
            SSQL = SSQL + " and ToDate='" + e.CommandArgument.ToString().Split(',').Last() + "' and WagesCode='" + e.CommandName.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);

            DataTable DT = new DataTable();

            SSQL = "Select EmpNo,Increment,SplIncrement from [" + SessionEpay + "]..EmpDaysIncrement where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " and FromDate='" + e.CommandArgument.ToString().Split(',').First() + "'";
            SSQL = SSQL + " and ToDate='" + e.CommandArgument.ToString().Split(',').Last() + "' and WagesCode='" + e.CommandName.ToString() + "' and Status='2'";
            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            for (int i = 0; i < DT.Rows.Count; i++)
            {
                SSQL = "";
                SSQL = "Update [" + SessionEpay + "]..EmpDaysIncrement set Status='1' where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and FromDate='" + e.CommandArgument.ToString().Split(',').First() + "'";
                SSQL = SSQL + " and ToDate='" + e.CommandArgument.ToString().Split(',').Last() + "' and WagesCode='" + e.CommandName.ToString() + "'";
                SSQL = SSQL + " and EmpNo='" + DT.Rows[i]["EmpNo"].ToString() + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);

                SSQL = "Update Employee_Mst set BaseSalary=BaseSalary + " + (Convert.ToDecimal(DT.Rows[i]["Increment"].ToString()) + Convert.ToDecimal(DT.Rows[i]["SplIncrement"].ToString())) + ",OT_Salary=OT_Salary + " + (Convert.ToDecimal(DT.Rows[i]["Increment"].ToString()) + Convert.ToDecimal(DT.Rows[i]["SplIncrement"].ToString())) + "";
                SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And EmpNo='" + DT.Rows[i]["EmpNo"].ToString() + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Increment Process Approved Successfully');", true);
        }
    }

    public void SalaryIncrement()
    {
        string Query = "";
        string MachineID = "";

        //Salary Increment Start
        DataTable DT_Emp = new DataTable();
        DataTable DT_Chk = new DataTable();
        DataTable DT = new DataTable();
        DataTable DT_Sal = new DataTable();
        DataTable DT_Inc = new DataTable();
        DataTable DT_Elg = new DataTable();
        string IncAmt = "0";
        string SplInc = "0";

        Query = "Select EmpNo,BaseSalary from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        //Query = Query + " And Wages='" + ddlWagesType.SelectedItem.Text + "' And IsActive='Yes' And Eligible_Increment='1'";
        Query = Query + " And Wages='" + ddlWagesType.SelectedItem.Text + "' And IsActive='Yes' ";
        DT_Elg = objdata.RptEmployeeMultipleDetails(Query);

        for (int i = 0; i < DT_Elg.Rows.Count; i++)
        {
            IncAmt = "0";
            SplInc = "0";
            MachineID = DT_Elg.Rows[i]["EmpNo"].ToString();

            string SSQL = "Select * from [" + SessionEpay + "]..MstDayIncProcess where Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And IncrementDate='" + txtIncDate.Text + "'";
            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count != 0)
            {
                SSQL = "Select * From [" + SessionEpay + "]..MstDaysSplIncrement where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                DT_Inc = objdata.RptEmployeeMultipleDetails(SSQL);
                if (DT_Inc.Rows.Count != 0)
                {
                    SplInc = DT_Inc.Rows[0]["SplIncrement"].ToString();
                }

                Query = "Select isnull(sum(Days),0) as EmpDays from [" + SessionEpay + "]..AttenanceDetails where EmpNo='" + MachineID + "' and ";
                Query = Query + "Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and convert(datetime, FromDate, 105 ) >= Convert(datetime, '" + DT.Rows[0]["FromDate"].ToString() + "',105) ";
                Query = Query + "and convert(datetime, ToDate, 105) <= convert(Datetime, '" + DT.Rows[0]["ToDate"].ToString() + "', 105) ";
                DT_Sal = objdata.RptEmployeeMultipleDetails(Query);

                if (DT_Sal.Rows.Count != 0)
                {
                    if (DT_Sal.Rows[0]["EmpDays"].ToString() != "" && DT_Sal.Rows[0]["EmpDays"].ToString() != "0")
                    {
                        Query = "Select isnull(IncrementAmt,'0') as IncrementAmt from [" + SessionEpay + "]..MstDaysIncrement where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                        Query = Query + " And PresentDays='" + DT_Sal.Rows[0]["EmpDays"].ToString() + "'";
                        DT_Inc = objdata.RptEmployeeMultipleDetails(Query);

                        if (DT_Inc.Rows.Count != 0)
                        {
                            IncAmt = DT_Inc.Rows[0]["IncrementAmt"].ToString();
                        }
                    }
                }

                Query = "Select * from [" + SessionEpay + "]..EmpDaysIncrement where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                Query = Query + " And FromDate='" + txtFromDate.Text + "' And ToDate='" + txtToDate.Text + "' And IncrementDate='" + txtIncDate.Text + "'";
                Query = Query + " And EmpNo='" + MachineID + "' And Status='2'";
                DT_Chk = objdata.RptEmployeeMultipleDetails(Query);

                decimal TotalInc = 0;
                decimal TotalBasic = 0;

                TotalInc = Convert.ToDecimal(SplInc) + Convert.ToDecimal(IncAmt);
                TotalInc = Math.Round(Convert.ToDecimal(TotalInc), 2, MidpointRounding.AwayFromZero);

                TotalBasic = Convert.ToDecimal(DT_Elg.Rows[i]["BaseSalary"].ToString()) + TotalInc;
                TotalBasic = Math.Round(Convert.ToDecimal(TotalBasic), 2, MidpointRounding.AwayFromZero);

                if (DT_Chk.Rows.Count == 0)
                {
                    Query = "Delete from [" + SessionEpay + "]..EmpDaysIncrement where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                    Query = Query + " And FromDate='" + txtFromDate.Text + "' And ToDate='" + txtToDate.Text + "' And IncrementDate='" + txtIncDate.Text + "'";
                    Query = Query + " And EmpNo='" + MachineID + "'";
                    objdata.RptEmployeeMultipleDetails(Query);

                    Query = "Insert into [" + SessionEpay + "]..EmpDaysIncrement (Ccode,Lcode,Days,FromDate,ToDate,";
                    Query = Query + "Increment,SplIncrement,TotalIncrement,TotalBasic,FinYear,Basic,EmpNo,Wages,WagesCode,IncrementDate,Status)values('" + SessionCcode + "',";
                    Query = Query + "'" + SessionLcode + "','" + DT_Sal.Rows[0]["EmpDays"].ToString() + "',";
                    Query = Query + "'" + txtFromDate.Text + "','" + txtToDate.Text + "',";
                    Query = Query + "'" + IncAmt + "','" + SplInc + "','" + TotalInc + "','" + TotalBasic + "','" + Convert.ToInt32(Utility.GetFinancialYear) + "-" + Convert.ToInt32(Utility.GetFinancialYear + 1) + "','" + DT_Elg.Rows[i]["BaseSalary"].ToString() + "','" + MachineID + "',";
                    Query = Query + "'" + ddlWagesType.SelectedItem.Text + "','" + ddlWagesType.SelectedValue + "',";
                    Query = Query + "'" + txtIncDate.Text + "','2')";
                    objdata.RptEmployeeMultipleDetails(Query);


                    //Query = "Update Employee_Mst set BaseSalary=BaseSalary + " + Convert.ToDecimal(DT_Inc.Rows[0]["IncrementAmt"].ToString()) + "";
                    //Query = Query + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    //Query = Query + " And Eligible_Increment='1' And EmpNo='" + MachineID + "'";
                    //objdata.RptEmployeeMultipleDetails(Query);
                }
            }
        }
        //Salary Increment End
    }

    protected void btnDownload_Click(object sender, EventArgs e)
    {
        if (txtFromDate.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Enter From Date');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Month", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlg = true;
            return;
        }
        if (txtToDate.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Enter To Date');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Month", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlg = true;
            return;
        }
        if (ddlWagesType.SelectedItem.Text == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Choose Wages');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Month", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlg = true;
            return;
        }
        if (!ErrFlg)
        {
            Load_Excel();
        }
    }

    private void Load_Excel()
    {
        try
        {
            DataTable DT_Elg = new DataTable();
            DataTable DT_Inc = new DataTable();
            DataTable dt = new DataTable();
            string SSQL = "";
            string SplInc = "0";

            SSQL = "Select ('&nbsp;'+EM.ExistingCode) as Code,EM.FirstName,EM.Wages,SUM(CAST(ED.Days as decimal(18,0))) as Days,EM.BaseSalary as Basic,SUM(ED.Increment) as Inc,Sum(ED.SplIncrement) as SplInc,(Cast(SUM(ED.Increment) as decimal(18,2))+Cast(SUm(ED.SplIncrement) as decimal(18,2))) as TotalInc,(CAST(EM.BaseSalary as decimal(18,2))+CAST(SUM(ED.Increment) as decimal(18,2))+CAST(SUM(ED.SplIncrement) as decimal(18,2))) as TotalBasic,EM.DeptName";
            SSQL = SSQL + " from [" + SessionEpay + "]..EmpDaysIncrement ED inner join Employee_Mst EM on EM.EmpNo=ED.EmpNo";
            SSQL = SSQL + " where Convert(datetime,FromDate,103)=Convert(datetime,'" + txtFromDate.Text + "',103) and Convert(datetime,ToDate,103)=Convert(datetime,'" + txtToDate.Text + "',103)";
            SSQL = SSQL + " and ED.Ccode='" + SessionCcode + "' and EM.CompCode='" + SessionCcode + "' and ED.Lcode='" + SessionLcode + "' and EM.LocCode='" + SessionLcode + "'";

            if (ddlWagesType.SelectedItem.Text != "-Select-")
            {
                SSQL = SSQL + " and ED.Wages='" + ddlWagesType.SelectedItem.Text + "' And EM.Wages='" + ddlWagesType.SelectedItem.Text + "'";
            }

            SSQL = SSQL + " group by EM.ExistingCode,EM.FirstName,EM.Wages,EM.DeptName,EM.BaseSalary order by EM.ExistingCode";

            dt = objdata.RptEmployeeMultipleDetails(SSQL);

            if (dt.Rows.Count > 0)
            {
                gv.DataSource = dt;
                gv.DataBind();
                string attachment = "attachment;filename=Increment_Check.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                gv.HeaderStyle.Font.Bold = true;
                System.IO.StringWriter stw = new System.IO.StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                gv.RenderControl(htextw);
                Response.Write(stw.ToString());
                Response.Flush();
                Response.End();
                Response.Clear();
            }
        }
        catch (Exception ex)
        {
            throw;
        }
    }
    //public override void VerifyRenderingInServerForm(Control control)
    //{
    //    /* Verifies that the control is rendered */
    //}

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        try
        {

            if (txtFromDate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Enter From Date');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Month", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlg = true;
                return;
            }
            if (txtToDate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Enter To Date');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Month", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlg = true;
                return;
            }
            if (txtIncDate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Enter Increment Date');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Month", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlg = true;
                return;
            }

            if (FileUpload.HasFile)
            {
                FileUpload.SaveAs(Server.MapPath("Upload/" + FileUpload.FileName));

            }
            if (!ErrFlg)
            {

                if (FileUpload.HasFile)
                {
                    FileUpload.SaveAs(Server.MapPath("Upload/" + FileUpload.FileName));

                }

                if (!ErrFlg)
                {
                    string connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath("Upload/" + FileUpload.FileName) + ";" + "Extended Properties=Excel 8.0;";
                    OleDbConnection sSourceConnection = new OleDbConnection(connectionString);
                    DataTable dts = new DataTable();
                    using (sSourceConnection)
                    {
                        sSourceConnection.Open();

                        OleDbCommand command = new OleDbCommand("Select * FROM [Sheet1$];", sSourceConnection);
                        sSourceConnection.Close();

                        using (OleDbCommand cmd = sSourceConnection.CreateCommand())
                        {
                            command.CommandText = "Select * FROM [Sheet1$];";
                            sSourceConnection.Open();
                        }
                        using (OleDbDataReader dr = command.ExecuteReader())
                        {

                            if (dr.HasRows)
                            {

                            }

                        }
                        OleDbDataAdapter objDataAdapter = new OleDbDataAdapter();
                        objDataAdapter.SelectCommand = command;
                        DataSet ds = new DataSet();

                        objDataAdapter.Fill(ds);
                        DataTable dt = new DataTable();
                        dt = ds.Tables[0];
                        for (int i = 0; i < dt.Columns.Count; i++)
                        {
                            dt.Columns[i].ColumnName = dt.Columns[i].ColumnName.Replace(" ", string.Empty).ToString();
                        }

                        for (int ck = 0; ck < dt.Rows.Count; ck++)
                        {
                            for (int j = 4; j < dt.Columns.Count; j++)
                                if (dt.Rows[ck][j].ToString() == string.Empty || dt.Rows[ck][j].ToString() == "" || dt.Rows[ck][j].ToString() == null)
                                {
                                    dt.Rows[ck][j] = "0";
                                    dt.AcceptChanges();
                                }
                        }

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {

                            string Empno = dt.Rows[i][0].ToString().Trim();

                            string Name = dt.Rows[i][1].ToString().Trim();
                            string Wages = dt.Rows[i][2].ToString().Trim();
                            decimal days = Convert.ToDecimal(dt.Rows[i][3].ToString());
                            decimal Basic = Convert.ToDecimal(dt.Rows[i][4].ToString());
                            decimal IncAmt = Convert.ToDecimal(dt.Rows[i][5].ToString());
                            decimal SplInc = Convert.ToDecimal(dt.Rows[i][6].ToString());
                            decimal TotalInc = Convert.ToDecimal(dt.Rows[i][7].ToString());
                            decimal TotalBasic = Convert.ToDecimal(dt.Rows[i][8].ToString());
                            string DeptName = dt.Rows[i][9].ToString();

                            string  SSQL = "";
                            SSQL = "Update [" + SessionEpay + "]..EmpDaysIncrement set Days='" + days + "', Basic='" + Basic + "',Increment='" + IncAmt + "',SplIncrement='" + SplInc + "'";
                            SSQL = SSQL + ",TotalIncrement='" + TotalInc + "',TotalBasic='" + TotalBasic + "',IncrementDate='" + txtIncDate.Text + "',Status='2'";
                            SSQL = SSQL + " where EmpNo='" + Empno + "' and Fromdate='" + txtFromDate.Text + "' and ToDate='" + txtToDate.Text + "'";
                            objdata.RptEmployeeMultipleDetails(SSQL);

                        }

                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Uploaded Successfully');", true);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Upload Correct File Format...');", true);
            //System.Windows.Forms.MessageBox.Show("Please Upload Correct File Format", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        }
    }
}
    
