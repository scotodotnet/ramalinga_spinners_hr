﻿<%@ Page Language="C#" MasterPageFile="~/PayrollMaster.master" AutoEventWireup="true" CodeFile="ELCLProcess.aspx.cs" Inherits="ELCLProcess" Title="" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
        if (sender._postBackSettings.panelsToUpdate != null) {
            $('.select2').select2();
                $('.datepicker').datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true
                });
            }
        });
    };
</script>
 <script type="text/javascript">
     function SaveMsgAlert(msg) {
         swal(msg);
     }
</script>

<script type="text/javascript">
    function ProgressBarShow() {
        $('#Download_loader').show();
    }
</script>

<script type="text/javascript">
    function ProgressBarHide() {
        $('#Download_loader').hide();
    }
</script>
 <!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Bonus</a></li>
				<li class="active">EL/CL Process</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">EL/CL Process</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
			 <asp:UpdatePanel ID="SalPay" runat="server">
                 <ContentTemplate>
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">EL/CL Process</h4>
                        </div>
                        <div class="panel-body">
                        <div class="form-group">
                       <!-- begin row -->
                        <div class="row">
                        <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Category</label>
								<asp:DropDownList runat="server" ID="ddlCategory" class="form-control select2" AutoPostBack="true"
                                        style="width:100%;" onselectedindexchanged="ddlCategory_SelectedIndexChanged1">
								 <asp:ListItem Value="0">-Select-</asp:ListItem>
								 <asp:ListItem Value="1">STAFF</asp:ListItem>
								 <asp:ListItem Value="2">LABOUR</asp:ListItem>
								</asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Employee Type</label>
								 <asp:DropDownList runat="server" ID="ddlEmployeeType" class="form-control select2" style="width:100%;">
								 </asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                            <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Bonus Year</label>
								 <asp:DropDownList runat="server" ID="ddlFinance" class="form-control select2" style="width:100%;">
								 </asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                           
                            <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								<br />
								 <asp:FileUpload ID="FileUpload" runat="server" />
								</div>
								</div>
								 <!-- end col-4 -->
                        </div>
                       <!-- end row -->
                     
                      
                       <!-- begin row -->  
                        <div class="row">
                         <div class="col-md-4">
                         
                         </div>
                           <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									<asp:Button ID="btnSalDown" runat="server" class="btn btn-warning" 
                                         Text="Download" onclick="btnSalDown_Click"   />
								       
                                         <asp:Button runat="server" class="btn btn-success" id="btnUpload" 
                                         Text="ELCL Process" onclick="btnUpload_Click" />
						    	 </div>
                               </div>
                              <!-- end col-4 -->
                              <div class="col-md-4"></div>
                               <tr id="GridView" runat="server" visible="false">
                                                                <td colspan="5">
                                                                    <asp:GridView ID="GridExcelView" runat="server" AutoGenerateColumns="true">
                                                                    </asp:GridView>
                                                                </td>
                                                            </tr>
                         </div>
                        <!-- end row --> 
                        
                    
                        </div>
                        </div>
                        
                    </div>
                    <!-- end panel -->
             </ContentTemplate>
               <Triggers>
                    <asp:PostBackTrigger ControlID="btnSalDown" />
                    <asp:PostBackTrigger ControlID="btnUpload" />
                 </Triggers>
             </asp:UpdatePanel>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->



</asp:Content>

