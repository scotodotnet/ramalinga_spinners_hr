﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using Payroll;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Globalization;

public partial class MstSplIncrement : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string SessionEpay;
    string SSQL = "";

    bool ErrFlg = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        SessionEpay = Session["SessionEpay"].ToString();

        if (!IsPostBack)
        {
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlFinYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }
            Load_Wages();
        }
        Load_OLD_Data();
    }

    private void Load_Wages()
    {

        DataTable dt = new DataTable();
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlWagesType.Items.Clear();
        query = "Select * from MstEmployeeType";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlWagesType.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpTypeCd"] = "0";
        dr["EmpType"] = "All";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlWagesType.DataTextField = "EmpType";
        ddlWagesType.DataValueField = "EmpTypeCd";
        ddlWagesType.DataBind();
    }

    private void Load_OLD_Data()
    {
        SSQL = "";
        SSQL = "Select * from [" + SessionEpay + "]..MstDaysSplIncrement where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        Repeater1.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater1.DataBind();
    }

    protected void btnEditEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "Select * from [" + SessionEpay + "]..MstDaysSplIncrement where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and WagesCode='" + e.CommandName + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count > 0)
        {
            txtIncrement.Text = dt.Rows[0]["SplIncrement"].ToString();
            ddlFinYear.SelectedValue = dt.Rows[0]["FinYear"].ToString();
            ddlWagesType.SelectedValue = dt.Rows[0]["WagesCode"].ToString();
            btnSave.Text = "Update";
        }
        Load_OLD_Data();
    }

    protected void btnDeleteEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "Delete from [" + SessionEpay + "]..MstDaysSplIncrement where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and WagesCode='" + e.CommandName + "'";
        objdata.RptEmployeeMultipleDetails(SSQL);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Increment Details Deleted Successfully');", true);
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
       
        if (txtIncrement.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Enter the Increment Amount');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Month", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlg = true;
            return;
        }
        if (ddlFinYear.SelectedItem.Text == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Enter the days');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Month", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlg = true;
            return;
        }
        if (!ErrFlg)
        {
            if (btnSave.Text == "Save")
            {
                SSQL = "";
                SSQL = "Select * from [" + SessionEpay + "]..MstDaysSplIncrement where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and WagesCode='" + ddlWagesType.SelectedValue + "'";
                DataTable dt = new DataTable();
                dt = objdata.RptEmployeeMultipleDetails(SSQL);

                if (dt.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('This Spl Increment Already Present');", true);
                    //System.Windows.Forms.MessageBox.Show("Select the Month", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlg = true;
                    return;
                }
            }
            if (!ErrFlg)
            {
                SSQL = "";
                SSQL = "Delete from [" + SessionEpay + "]..MstDaysSplIncrement where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and (WagesCode='" + ddlWagesType.SelectedValue + "' or WagesCode='0')";
                objdata.RptEmployeeMultipleDetails(SSQL);

                if (ddlWagesType.SelectedValue == "0")
                {
                    SSQL = "";
                    SSQL = "Delete from [" + SessionEpay + "]..MstDaysSplIncrement where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                }

                SSQL = "";
                SSQL = "Insert into [" + SessionEpay + "]..MstDaysSplIncrement (Ccode,Lcode,FinYear,SplIncrement,Wages,WagesCode) ";
                SSQL = SSQL + "Values('" + SessionCcode + "','" + SessionLcode + "','" + ddlFinYear.SelectedValue + "','" + txtIncrement.Text + "','" + ddlWagesType.SelectedItem.Text + "','" + ddlWagesType.SelectedValue + "')";
                objdata.RptEmployeeMultipleDetails(SSQL);

                if (btnSave.Text == "Save")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Increment Details Saved Successfully');", true);

                }
                if (btnSave.Text == "Update")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Increment Details Updated Successfully');", true);
                    //btnCancel_Click(sender, e);
                }
                btnCancel_Click(sender, e);
            }
        }
        Load_OLD_Data();
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        txtIncrement.Text = "";
        ddlWagesType.ClearSelection();
        ddlFinYear.ClearSelection();
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("MstIncrementNew.aspx");
    }
}