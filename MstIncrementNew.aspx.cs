﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using Payroll;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Globalization;

public partial class MstIncrementNew : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string SessionEpay;
    string SSQL = "";

    bool ErrFlg = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        SessionEpay = Session["SessionEpay"].ToString();

        if (!IsPostBack)
        {
            //int currentYear = Utility.GetFinancialYear;
            //for (int i = 0; i < 10; i++)
            //{
            //    ddlFinYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
            //    //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
            //    currentYear = currentYear - 1;
            //}

            Load_Data();
        }
        Load_OLD_Data();
    }

    private void Load_OLD_Data()
    {
        SSQL = "";
        SSQL = "Select * from [" + SessionEpay + "]..MstDaysIncrement where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        Repeater1.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater1.DataBind();
    }


    private void Load_Data()
    {
        DataTable DT = new DataTable();
        SSQL = "";
        SSQL = "Select * from [" + SessionEpay + "]..MstDaysSplIncrement where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (DT.Rows.Count != 0)
        {
            txtSplIncrement.Text = DT.Rows[0]["SplIncrement"].ToString();
        }
        else
        {
            txtSplIncrement.Text = "0";
        }
    }


    protected void ddlFinYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (txtDays.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Enter the days');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Month", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlg = true;
            return;
        }
        if (txtIncrement.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Enter the Increment Amount');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Month", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlg = true;
            return;
        }
        if (txtSplIncrement.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Enter the Spl Increment');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Month", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlg = true;
            return;
        }
        //if (ddlFinYear.SelectedItem.Text == "-Select-")
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Enter the days');", true);
        //    //System.Windows.Forms.MessageBox.Show("Select the Month", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        //    ErrFlg = true;
        //    return;
        //}
        if (!ErrFlg)
        {
            if (btnSave.Text == "Save")
            {
                SSQL = "";
                SSQL = "Select * from [" + SessionEpay + "]..MstDaysIncrement where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and PresentDays='" + txtDays.Text.ToString() + "'";
                DataTable dt = new DataTable();
                dt = objdata.RptEmployeeMultipleDetails(SSQL);

                if (dt.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('This Days Amount Already Present');", true);
                    //System.Windows.Forms.MessageBox.Show("Select the Month", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlg = true;
                    return;
                }
            }
            if (!ErrFlg)
            {
                SSQL = "";
                SSQL = "Delete from [" + SessionEpay + "]..MstDaysIncrement where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and PresentDays='" + txtDays.Text.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);

                SSQL = "";
                SSQL = "Insert into [" + SessionEpay + "]..MstDaysIncrement (Ccode,Lcode,PresentDays,IncrementAmt,SplIncrement) ";
                SSQL = SSQL + "Values('" + SessionCcode + "','" + SessionLcode + "','" + txtDays.Text + "','" + txtIncrement.Text + "','" + txtSplIncrement.Text + "')";
                objdata.RptEmployeeMultipleDetails(SSQL);

                if (btnSave.Text == "Save")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Increment Details Saved Successfully');", true);
                    
                }
                if (btnSave.Text == "Update")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Increment Details Updated Successfully');", true);
                    //btnCancel_Click(sender, e);
                }
                btnCancel_Click(sender, e);
            }
        }
        Load_OLD_Data();
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        txtDays.Text = "0.0";
        txtIncrement.Text = "0.0";
        txtSplIncrement.Text = "0.0";
        //ddlFinYear.ClearSelection();
        btnSave.Text="Save";
    }

    protected void btnEditEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "Select * from [" + SessionEpay + "]..MstDaysIncrement where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and PresentDays='" + e.CommandName + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count > 0)
        {
            txtDays.Text = dt.Rows[0]["PresentDays"].ToString();
            txtIncrement.Text = dt.Rows[0]["IncrementAmt"].ToString();
            //txtSplIncrement.Text= dt.Rows[0]["SplIncrement"].ToString();
            //ddlFinYear.SelectedValue= dt.Rows[0]["FinYearCode"].ToString();
            btnSave.Text = "Update";
        }
        Load_OLD_Data();
    }

    protected void btnDeleteEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "Delete from [" + SessionEpay + "]..MstDaysIncrement where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and PresentDays='" + e.CommandName.ToString() + "'";
        objdata.RptEmployeeMultipleDetails(SSQL);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Increment Details Deleted Successfully');", true);

        Load_OLD_Data();
    }

    protected void btnSplincrement_Click(object sender, EventArgs e)
    {
        //Response.Redirect("MstSplIncrement.aspx");

        SSQL = "";
        SSQL = "Delete from [" + SessionEpay + "]..MstDaysSplIncrement where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        objdata.RptEmployeeMultipleDetails(SSQL);

        SSQL = "";
        SSQL = "Insert into [" + SessionEpay + "]..MstDaysSplIncrement (Ccode,Lcode,SplIncrement) ";
        SSQL = SSQL + "Values('" + SessionCcode + "','" + SessionLcode + "','" + txtSplIncrement.Text + "')";
        objdata.RptEmployeeMultipleDetails(SSQL);

        Load_Data();
    }

}