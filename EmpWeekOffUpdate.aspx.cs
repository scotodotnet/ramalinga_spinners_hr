﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using System.IO;
using Payroll;
using AjaxControlToolkit;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
public partial class EmpWeekOffUpdate : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights; string SessionAdmin;
    string SSQL;
    bool ErrFlg = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();


        if (!IsPostBack)
        {
            int currentYear =DateTime.Now.Year;
            for (int i = 0; i < 10; i++)
            {
                ddlFinYearFrom.Items.Add(new ListItem(Convert.ToString(currentYear).ToString() , (currentYear).ToString()));
                ddlFinYearTo.Items.Add(new ListItem(currentYear.ToString(), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }

            Page.Title = "HR Module :: Employee Change Slat";
            Load_Type();
            Load_Data();
        }
    }

    private void Load_Data()
    {
        SSQL = "";
        SSQL = "Select * from WeekOffUpdate where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and ChangeYear=year(getdate())";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }

    private void Load_Type()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlWagesType.Items.Clear();
        query = "Select *from MstEmployeeType";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlWagesType.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpType"] = "-Select-";
        dr["EmpType"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlWagesType.DataTextField = "EmpType";
        ddlWagesType.DataValueField = "EmpType";
        ddlWagesType.DataBind();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (ddlFromWeekOff.SelectedItem.Text == "-Select-")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the Week OFF From');", true);
            return;
        }

        if (ddlToWeekOff.SelectedItem.Text == "-Select-")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the Week OFF To');", true);
            return;
        }
        if (ddlFromWeekOff.SelectedItem.Text == ddlToWeekOff.SelectedItem.Text)
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select Differention of From and To Weekoff');", true);
            return;
        }
        if (!ErrFlg)
        {

            SSQL = "";
            SSQL = "Select * from Employee_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
            SSQL = (ddlWagesType.SelectedItem.Text != "-Select-") ? SSQL + "and Wages='" + ddlWagesType.SelectedItem.Text + "'" : SSQL;
            SSQL = SSQL + " and  WeekOff='" + ddlFromWeekOff.SelectedItem.Text + "'";
            SSQL = SSQL + " and WHChangeYr='" + ddlFinYearFrom.SelectedValue + "'";
            DataTable dt = new DataTable();
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            //  ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "alert", "ProgressBarShow();", true);
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    SSQL = "";
                    SSQL = "Select * from WeekOffUpdate where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and EmpNo='" + dt.Rows[i]["EmpNo"].ToString() + "'";
                    SSQL = SSQL + " and [To]='" + ddlFromWeekOff.SelectedItem.Text + "' and ChangeYear='" + ddlFinYearTo.SelectedValue + "'";
                    DataTable dt_check = new DataTable();
                    dt_check = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dt_check.Rows.Count == 0)
                    {
                        SSQL = "";
                        SSQL = "Insert Into WeekOffUpdate(Ccode,Lcode,EmpType,[From],[To],ChangeBy,ChangedOn,EmpNo,ExistingCode,ChangeYear) values('" + SessionCcode + "','" + SessionLcode + "','" + dt.Rows[i]["Wages"].ToString() + "','" + ddlFromWeekOff.SelectedItem.Text + "','" + ddlToWeekOff.Text + "'";
                        SSQL = SSQL + ",'" + SessionUserName + "',Convert(varchar,getDate(),103),'" + dt.Rows[i]["EmpNo"].ToString() + "','" + dt.Rows[i]["ExistingCode"].ToString() + "','" + ddlFinYearTo.SelectedValue + "')";
                        objdata.RptEmployeeMultipleDetails(SSQL);

                        SSQL = "";
                        SSQL = "Update Employee_Mst set WeekOff='" + ddlToWeekOff.SelectedItem.Text + "', WHChangeYr='" + ddlFinYearTo.SelectedValue + "'";
                        SSQL = SSQL + " where  EmpNo='" + dt.Rows[i]["EmpNo"].ToString() + "'";
                        SSQL = SSQL + " and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                        objdata.RptEmployeeMultipleDetails(SSQL);
                    }
                }
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "ProgressBarHide();", true);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Records Not Found!!!');", true);
            }

            //objdata.RptEmployeeMultipleDetails(SSQL);

            ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "ProgressBarHide();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Transfer Completed!!!');", true);

            // ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Transfer Completed!!!');", true);
            btnClear_Click(sender, e);
            Load_Data();
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        ddlWagesType.ClearSelection();
        ddlFromWeekOff.ClearSelection();
        ddlToWeekOff.ClearSelection();
    }
}