﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Globalization;
public partial class ESM_Dummy_rpt : System.Web.UI.Page
{
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    ReportDocument rd = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    SqlConnection con;
    string str_month;
    string str_yr;
    string str_cate;
    string str_dept;
    string AgentName;
    string SessionCcode;
    string SessionLcode;
    string SessionAdmin; string SessionUserType;
    static string CmpName;
    static string Cmpaddress;
    string query;
    string fromdate;
    string ToDate;
    string salaryType;
    string Emp_ESI_Code;
    string SessionPayroll;
    string EmployeeType; string EmployeeTypeCd;
    string PayslipType;
    string Report_Types;

    string PFTypeGet;

    string Left_Employee = "0";
    string Left_Date = "";
    string Get_Report_Type = "0";
    string Salary_CashOrBank;
    string Get_Division_Name = "";

    string Other_state = "";
    string Non_Other_state = "";
    string Basic_Report_Date = "";
    string Basic_Report_Type = "OLD";
    string ExemptedStaff = "";
    string SSQL = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        Load_rpt();
    }

    private void Load_rpt()
    {
        SSQL = "";
        SSQL = "Select * from [Dummy1]..Dummytbl";
        DataTable Dt = new DataTable();
        Dt = objdata.RptEmployeeMultipleDetails(SSQL);

        if (Dt != null && Dt.Rows.Count > 0)
        {
            rd.Load(Server.MapPath("Payslip_New_Component/PaySlipModule_ESM.rpt"));
            rd.DataDefinition.FormulaFields["monthYear"].Text = "'"+ "Sep 2019" + "'";
            rd.Database.Tables[0].SetDataSource(Dt);
            rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = rd;
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Records Found');", true);
        }
    }
}