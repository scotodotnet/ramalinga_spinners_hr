﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using System.Data.SqlClient;
using System.IO;
using System.Net.Mail;
using System.Drawing;

public partial class EmpPromotion : System.Web.UI.Page
{
    System.Web.UI.WebControls.DataGrid grid =
                      new System.Web.UI.WebControls.DataGrid();
    String CurrentYear1;
    static int CurrentYear;
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionEpay;
    string SessionUserName;
    string SessionLcode; string SessionUser;
    string SessionDivision; string SessionRights;
    BALDataAccess objdata = new BALDataAccess();
    string SSQL = "";
    string ss1 = "";
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;
    string SessionUserType;
    string Date1_str;
    string Date2_str;
    string Wages_Type;
    bool Errflag = false;
    bool Errflag1 = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            SessionRights = Session["Rights"].ToString();
            SessionEpay = Session["SessionEpay"].ToString();
            SessionUserName = Session["Usernmdisplay"].ToString();
            SessionUser = Session["Usernmdisplay"].ToString();
            txtPromotionTo.Visible = false;

            con = new SqlConnection(constr);
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Employee Promotion";

                Load_MachineID();
              

            }
            Load_Data();
        }
    }

    private void Load_Data()
    {
        SSQL = "";
        SSQL = "Select * from Promotion_Mst";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }


    private void Load_MachineID()
    {
        SSQL = "";
        SSQL = "Select MachineID from Employee_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and IsActive='Yes'";
        ddlMachineID.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlMachineID.DataTextField = "MachineID";
        ddlMachineID.DataValueField = "MachineID";
        ddlMachineID.DataBind();
        ddlMachineID.Items.Insert(0, new ListItem("-Select-", "-Select-"));
    }

    protected void ddlCategoryFrom_SelectedIndexChanged(object sender, EventArgs e)
    {
        SSQL = "";
        SSQL = "Select * from MstEmployeeType where EmpCategory='" + ddlCategoryFrom.SelectedValue + "'";
        ddlEmployeeTypeFrom.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlEmployeeTypeFrom.DataTextField = "EmpType";
        ddlEmployeeTypeFrom.DataValueField = "EmpTypeCd";
        ddlEmployeeTypeFrom.DataBind();
        ddlEmployeeTypeFrom.Items.Insert(0, new ListItem("-Select-", "-Select-"));
    }
    protected void ddlCategoryTo_SelectedIndexChanged(object sender, EventArgs e)
    {
        SSQL = "";
        SSQL = "Select * from MstEmployeeType where EmpCategory='" + ddlCategoryTo.SelectedValue + "'";
        ddlEmployeeTypeTo.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlEmployeeTypeTo.DataTextField = "EmpType";
        ddlEmployeeTypeTo.DataValueField = "EmpType";
        ddlEmployeeTypeTo.DataBind();
        ddlEmployeeTypeTo.Items.Insert(0, new ListItem("-Select-", "-Select-"));
    }

    protected void ddlMachineID_SelectedIndexChanged(object sender, EventArgs e)
    {
        SSQL = "";
        SSQL = "Select FirstName,DeptName,Designation,ExistingCode,CatName,Wages from Employee_Mst where MachineID='" + ddlMachineID.SelectedValue + "' and CompCode='" + SessionCcode + "'";
        SSQL = SSQL + " and LocCode='" + SessionLcode + "'";
        DataTable Dt_Details = new DataTable();
        Dt_Details = objdata.RptEmployeeMultipleDetails(SSQL);
        if (Dt_Details != null && Dt_Details.Rows.Count > 0)
        {
            txtName.Text = Dt_Details.Rows[0]["FirstName"].ToString();
            txtExistingCodeFrom.Text = Dt_Details.Rows[0]["ExistingCode"].ToString();
            txtDeptName.Text= Dt_Details.Rows[0]["DeptName"].ToString();
            txtDesignation.Text = Dt_Details.Rows[0]["Designation"].ToString();
            ddlCategoryFrom.SelectedItem.Text= Dt_Details.Rows[0]["CatName"].ToString();
            ddlCategoryFrom_SelectedIndexChanged(sender,e);
            ddlEmployeeTypeFrom.SelectedItem.Text = Dt_Details.Rows[0]["Wages"].ToString();
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (ddlMachineID.SelectedItem.Text == "-Select-")
        {
            Errflag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the MachineID')", true);
            return;
        }
        if (txtExistingCodeTo.Text == "")
        {
            Errflag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the ExistingCode')", true);
            return;
        }
        if (ddlCategoryTo.SelectedItem.Text == "-Select-")
        {
            Errflag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the new Category')", true);
            return;
        }
        if (ddlEmployeeTypeTo.SelectedItem.Text == "-Select-")
        {
            Errflag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the new EmloyeeType')", true);
            return;
        }
        if (!Errflag)
        {
            SSQL = "";
            SSQL = "Update Employee_Mst set ExistingCode='" + txtExistingCodeTo.Text.Trim() + "',CatName='" + ddlCategoryTo.SelectedItem.Text + "' ,Wages='" + ddlEmployeeTypeTo.SelectedItem.Text + "'";
            SSQL = SSQL + " where MachineID='" + ddlMachineID.SelectedItem.Text + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "";
            SSQL = "Update LogTime_Days Set ExistingCode='" + txtExistingCodeTo.Text.Trim() + "', Wages='" + ddlEmployeeTypeTo.SelectedItem.Text + "'";
            SSQL = SSQL + " where MachineID='" + ddlMachineID.SelectedItem.Text + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "";
            SSQL = "Update [" + SessionEpay + "]..AttenanceDetails set ExistingCode='" + txtExistingCodeTo.Text.Trim() + "'";
            SSQL = SSQL + " where EmpNo='" + ddlMachineID.SelectedItem.Text + "' and CCode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "";
            SSQL = "Update [" + SessionEpay + "]..SalaryDetails set ExisistingCode='" + txtExistingCodeTo.Text.Trim() + "'";
            SSQL = SSQL + " where EmpNo='" + ddlMachineID.SelectedItem.Text + "' and CCode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);

            //change Start 29/10/2019
            SSQL = "";
            SSQL = "Insert Into Promotion_Mst(MachineId,EmpName,DeptName,Designation,ExistingCode_Frm,Category_Frm,Emp_Type_Frm, ";
            SSQL = SSQL + "ExistingCode_To,Category_To,Emp_type_To,Created_by,Promoted_date,Created_date)Values(";
            SSQL = SSQL + "'" + ddlMachineID.SelectedItem.Text + "','" + txtName.Text + "','" + txtDeptName.Text + "','" + txtDesignation.Text + "',";
            SSQL = SSQL + "'" + txtExistingCodeFrom.Text + "','" + ddlCategoryFrom.SelectedItem.Text + "','" + ddlEmployeeTypeFrom.SelectedItem.Text + "',";
            SSQL = SSQL + "'" + txtExistingCodeTo.Text.Trim() + "','" + ddlCategoryTo.SelectedItem.Text + "','" + ddlEmployeeTypeTo.SelectedItem.Text + "',";
            SSQL = SSQL + "'" + SessionUserName + "',convert(varchar,GetDate(),105),GetDate())";
            objdata.RptEmployeeMultipleDetails(SSQL);


            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Employee Promoted Successfully')", true);
            btnClr_Click(sender, e);
            Load_Data();

        }
    }

    protected void btnClr_Click(object sender, EventArgs e)
    {
        ddlMachineID.ClearSelection();
        txtName.Text = "";
        txtDeptName.Text = "";
        txtDesignation.Text = "";
        txtExistingCodeFrom.Text = "";
        ddlCategoryFrom.ClearSelection();
        ddlEmployeeTypeFrom.ClearSelection();
        ddlEmployeeTypeTo.ClearSelection();
        ddlCategoryTo.ClearSelection();
        txtExistingCodeTo.Text = "";
        txtPromotionTo.Text = "";
    }

    protected void txtExistingCodeTo_TextChanged(object sender, EventArgs e)
    {
        SSQL = "";
        SSQL = "Select * from Employee_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and ExistingCode='" + txtExistingCodeTo.Text.Trim() + "'";
        DataTable dt_Check = new DataTable();
        dt_Check = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt_Check != null && dt_Check.Rows.Count > 0)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('This Existing Code is already Present')", true);
            txtExistingCodeTo.Focus();
            txtExistingCodeTo.Text = "";
            return;
        }
    }

    //protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    //{
    //    string query = "";
    //    DataTable DT_Details = new DataTable();
    //    query = "Select * from Promotion_Mst where MachineId='" + e.CommandName.ToString() + "'";
    //    DT_Details = objdata.RptEmployeeMultipleDetails(query);

    //    if (DT_Details.Rows.Count != 0)
    //    {
           
            
    //      txtName.Text = DT_Details.Rows[0]["EmpName"].ToString();
    //        txtDeptName.Text = DT_Details.Rows[0]["DeptName"].ToString();
    //        txtDesignation.Text = DT_Details.Rows[0]["Designation"].ToString();
    //        txtExistingCodeFrom.Text = DT_Details.Rows[0]["ExistingCode_Frm"].ToString();
    //        ddlCategoryFrom.SelectedItem.Text = DT_Details.Rows[0]["Category_Frm"].ToString();
    //        ddlCategoryFrom_SelectedIndexChanged(sender, e);
    //        ddlEmployeeTypeFrom.SelectedItem.Text = DT_Details.Rows[0]["Emp_Type_Frm"].ToString();
    //        txtExistingCodeTo.Text = DT_Details.Rows[0]["ExistingCode_To"].ToString();
    //      //  ddlEmployeeTypeTo.SelectedItem.Text = DT_Details.Rows[0]["Emp_type_To"].ToString();
    //        ddlCategoryTo.SelectedItem.Text = DT_Details.Rows[0]["Category_To"].ToString();
    //    }
    //    else
    //    {
    //        //ddlYear.SelectedValue = "-Select-";
    //        //txtDesc.Text = "";
    //        //ddlNFHType.SelectedValue = "-Select-";
    //        //chkDoubleWages.Checked = false;
    //        //chkDoubleWages.Enabled = false;
    //        //chkForm25.Checked = false;
    //        //txtForm25Text.Text = "";
    //        //txtID.Value = "";
    //        //btnSave.Text = "Save";
    //    }
    //}

}