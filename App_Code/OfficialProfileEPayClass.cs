﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for OfficialProfileEPayClass
/// </summary>
public class OfficialProfileEPayClass
{
	public OfficialProfileEPayClass()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    private string _TokenNo;

    public string TokenNo
    {
        get { return _TokenNo; }
        set { _TokenNo = value; }
    }

    private string _MachineID;

    public string MachineID
    {
        get { return _MachineID; }
        set { _MachineID = value; }
    }

    private string _DOJ;

    public string DOJ
    {
        get { return _DOJ; }
        set { _DOJ = value; }
    }

    private string _ESINumber;

    public string ESINumber
    {
        get { return _ESINumber; }
        set { _ESINumber = value; }
    }

    private string _PFNumber;

    public string PFNumber
    {
        get { return _PFNumber; }
        set { _PFNumber = value; }
    }

    private string _BranchCode;

    public string BranchCode
    {
        get { return _BranchCode; }
        set { _BranchCode = value; }
    }

    private string _BankName;

    public string BankName
    {
        get { return _BankName; }
        set { _BankName = value; }
    }

    private string _AccountNo;

    public string AccountNo
    {
        get { return _AccountNo; }
        set { _AccountNo = value; }
    }

    private string _WagesType;

    public string WagesType
    {
        get { return _WagesType; }
        set { _WagesType = value; }
    }

    private string _Basic;

    public string Basic
    {
        get { return _Basic; }
        set { _Basic = value; }
    }

    private string _ESIEligible;

    public string ESIEligible
    {
        get { return _ESIEligible; }
        set { _ESIEligible = value; }
    }

    private string _PFEligible;

    public string PFEligible
    {
        get { return _PFEligible; }
        set { _PFEligible = value; }
    }

    private string _Ccode;

    public string Ccode
    {
        get { return _Ccode; }
        set { _Ccode = value; }
    }

    private string _Lcode;

    public string Lcode
    {
        get { return _Lcode; }
        set { _Lcode = value; }
    }

    private string _ESICode;

    public string ESICode
    {
        get { return _ESICode; }
        set { _ESICode = value; }
    }

    private string _PFdoj;

    public string PFdoj
    {
        get { return _PFdoj; }
        set { _PFdoj = value; }
    }

    private string _ESIdoj;

    public string ESIdoj
    {
        get { return _ESIdoj; }
        set { _ESIdoj = value; }
    }
}
