﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="MstOTIncentive.aspx.cs" Inherits="MstOTIncentive" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('.select2').select2();
                    $('.datepicker').datepicker({
                        format: "dd/mm/yyyy",
                        autoclose: true
                    });
                }
            });
        };
    </script>
     <script type="text/javascript">
     $(document).ready(function() {
         $('#example').dataTable();
     });
	</script>


<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
                $('.select2').select2();
            }
        });
    };
</script>
    <script type="text/javascript">
        function SaveMsgAlert(msg) {
            swal(msg);
        }
    </script>

    <script type="text/javascript">
        function ProgressBarShow() {
            $('#Download_loader').show();
        }
    </script>

    <script type="text/javascript">
        function ProgressBarHide() {
            $('#Download_loader').hide();
        }
    </script>
    <!-- begin #content -->
    <div id="content" class="content">
        <!-- begin breadcrumb -->
        <ol class="breadcrumb pull-right">
            <li><a href="javascript:;">Additional Incentive</a></li>
            <%--<li class="active">Salary Calculation</li>--%>
        </ol>
        <!-- end breadcrumb -->
        <!-- begin page-header -->
        <h1 class="page-header">Additional Incentive</h1>
        <!-- end page-header -->

        <!-- begin row -->
        <div class="row">
            <!-- begin col-12 -->
            <asp:UpdatePanel ID="SalPay" runat="server">
                <ContentTemplate>
                    <div class="col-md-12">
                        <!-- begin panel -->
                        <div class="panel panel-inverse">
                            <div class="panel-heading">
                                <div class="panel-heading-btn">
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                                </div>
                                <h4 class="panel-title">Additional Incentive</h4>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3" runat="server" visible="false">
                                            <div class="form-group">
                                                <label>OT Hrs From (HH.mm)</label>
                                                <asp:TextBox runat="server" ID="txtOTHrsFm" Text="00.00" class="form-control"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender runat="server" ID="FilteredTextBoxExtender2" TargetControlID="txtOTHrsFm" ValidChars="." FilterType="Numbers,Custom">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>
                                        </div>

                                         <div class="col-md-3" runat="server" visible="false">
                                             <div class="form-group">
                                                <label>OT Hrs To (HH.mm)</label>
                                                <asp:TextBox runat="server" ID="txtOTHrsTo" Text="00.00" class="form-control"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender runat="server" ID="FilteredTextBoxExtender3" TargetControlID="txtOTHrsTo" ValidChars="." FilterType="Numbers,Custom">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>
                                        </div>
                                       
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                 <label>OT Incentive Amt</label>
                                                <asp:TextBox runat="server" ID="txtOTAmt"  Text="0" class="form-control"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender runat="server" ID="FilteredTextBoxExtender1" TargetControlID="txtOTAmt" ValidChars="." FilterType="Numbers,Custom">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>
                                        </div>
                                         <div class="col-md-3"></div>
                                          <div class="col-md-3">
                                            <div class="form-group">
                                                 <label>WeekOff Incentive Amt</label>
                                                <asp:TextBox runat="server" ID="txtWeekAmt"  Text="0" class="form-control"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender runat="server" ID="FilteredTextBoxExtender4" TargetControlID="txtWeekAmt" ValidChars="." FilterType="Numbers,Custom">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- begin row -->
                                    <div class="row">
                                        <div class="col-md-4"></div>
                                        <!-- begin col-4 -->
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <br />
                                                <asp:Button runat="server" ID="btnSave" Text="Save" class="btn btn-success"
                                                    OnClick="btnSave_Click" />
                                              
                                                <asp:Button ID="btnCancel" runat="server" class="btn btn-warning"
                                                    Text="Cancel" OnClick="btnCancel_Click" />
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <div class="col-md-4"></div>
                                    </div>
                                    <div class="row">
                                        <!-- table start -->
                                        <div class="col-md-12" runat="server" visible="false">
                                            <div class="row">
                                                <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
                                                    <HeaderTemplate>
                                                        <table id="example" class="display table">
                                                            <thead>
                                                                <tr>
                                                                    <th>S.No</th>
                                                                    <th>OT.Hrs From</th>
                                                                    <th>OT.Hrs To</th>
                                                                    <th>OT.Amt</th>
                                                                    <th>Mode</th>
                                                                </tr>
                                                            </thead>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td><%# Container.ItemIndex + 1 %></td>
                                                            <td><%# Eval("othrsfm")%></td>
                                                            <td><%# Eval("othrsto")%></td>
                                                            <td><%# Eval("OTAmt")%></td>
                                                            <td>
                                                                <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-success btn-sm fa fa-pencil" runat="server"
                                                                    Text="" OnCommand="btnEditEnquiry_Grid_Command" CommandArgument="Edit" CommandName='<%# Eval("OTHrsFm")+","+Eval("OTHrsTo")%>'>
                                                                </asp:LinkButton>
                                                                <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                     Text="" OnCommand="btnDeleteEnquiry_Grid_Command" CommandArgument="Delete" CommandName='<%# Eval("OTHrsFm")+","+Eval("OTHrsTo")%>'
                                                                    CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this details?');">
                                                                </asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate></table></FooterTemplate>
                                                </asp:Repeater>
                                            </div>
                                        </div>
                                        <!-- table End -->
                                    </div>
                                </div>
                                <div id="Download_loader" style="display: none" />
                            </div>
                        </div>
                        <!-- end panel -->
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnSave" />
                    <asp:PostBackTrigger ControlID="btnCancel" />
                </Triggers>
            </asp:UpdatePanel>
            <!-- end col-12 -->
        </div>
        <!-- end row -->
    </div>
    <!-- end #content -->
</asp:Content>
