<%@ Page Language="C#" MasterPageFile="~/PayrollMaster.master" AutoEventWireup="true" CodeFile="AgentAndMess.aspx.cs" Inherits="AgentAndMess" Title="   " %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:UpdatePanel ID="SalPay" runat="server">
                 <ContentTemplate>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
        if (sender._postBackSettings.panelsToUpdate != null) {
            $('.select2').select2();
                $('.datepicker').datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true
                });
            }
        });
    };
</script>


 <!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Report</a></li>
				<li class="active">Agent & Mess</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Agent & Mess</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Agent & Mess</h4>
                        </div>
                        <div class="panel-body">
                        <div class="form-group">
                        <!-- begin row -->
                        <div class="row">
                         <!-- begin col-4 -->
                              <div class="col-md-3">
								<div class="form-group">
								 <label>Report Type</label>
                             <asp:RadioButtonList ID="rbtnReportType" runat="server" class="form-control"  style="width:100%;"
                                        RepeatDirection="Horizontal" AutoPostBack="true"
                                        onselectedindexchanged="rbtnReportType_SelectedIndexChanged">
                             <asp:ListItem Value="1" Text="Agent" Selected="True"></asp:ListItem>
                             <asp:ListItem Value="2" Text="Mess"></asp:ListItem>
                             </asp:RadioButtonList>
								 
								 
								
								</div>
                               </div>
                           <!-- end col-4 -->
                            <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Agent Name</label>
								 <asp:DropDownList runat="server" ID="ddlAgentName" class="form-control select2" style="width:100%;">
								 </asp:DropDownList>
								
								</div>
                               </div>
                           <!-- end col-4 -->
                             <div class="col-md-4">
								<div class="form-group">
								 <label>Fin. Year</label>
								 <asp:DropDownList runat="server" ID="ddlFinance" class="form-control select2" style="width:100%;">
							 	 </asp:DropDownList>
								</div>
                               </div>
                        
                        </div>
                    <!-- end row -->
                       <!-- begin row -->
                        <div class="row">
                         <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Month</label>
								 <asp:DropDownList runat="server" ID="ddlMonths" class="form-control select2" style="width:100%;">
							 	 </asp:DropDownList>
							 	 <asp:RequiredFieldValidator ControlToValidate="ddlMonths" InitialValue="0" Display="Dynamic"  ValidationGroup="Validate_PayField" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ControlToValidate="ddlMonths" InitialValue="0" Display="Dynamic"  ValidationGroup="Validate_ExpField" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ControlToValidate="ddlMonths" InitialValue="0" Display="Dynamic"  ValidationGroup="Validate_LeaveField" class="form_error" ID="RequiredFieldValidator9" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ControlToValidate="ddlMonths" InitialValue="0" Display="Dynamic"  ValidationGroup="Validate_ManField" class="form_error" ID="RequiredFieldValidator12" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
								</div>
                               </div>
                           <!-- end col-4 -->
                         <!-- begin col-4 -->
                              <div class="col-md-2" runat="server" id="IF_FromDate_Hide">
								<div class="form-group">
								 <label>FromDate</label>
								  <asp:TextBox ID="txtfrom" runat="server" class="form-control datepicker" placeholder="dd/MM/YYYY"></asp:TextBox>
                                   <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                        TargetControlID="txtfrom" ValidChars="0123456789/">
                                   </cc1:FilteredTextBoxExtender>
								</div>
                               </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                              <div class="col-md-2" runat="server" id="IF_ToDate_Hide">
								<div class="form-group">
								 <label>ToDate</label>
								 <asp:TextBox ID="txtTo" runat="server" class="form-control datepicker" placeholder="dd/MM/YYYY"></asp:TextBox>
                                   <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                        TargetControlID="txtTo" ValidChars="0123456789/">
                                   </cc1:FilteredTextBoxExtender>
								</div>
                               </div>
                           <!-- end col-4 -->
                           <div class="col-md-4">
                               <br />
									<asp:Button runat="server" id="btnSearch" Text="Agent Report" 
                                   class="btn btn-success" ValidationGroup="Validate_PayField" 
                                   onclick="btnSearch_Click1"/>
									<asp:Button runat="server" id="btnExport" Text="Mess Report" 
                                   class="btn btn-success" ValidationGroup="Validate_ExpField" 
                                   onclick="btnExport_Click1"  />
                               </div>
                           
                            <!-- begin col-4 -->
                            
                               
                           <!-- end col-4 -->
                        </div>
                       <!-- end row -->
                        <!-- begin row -->
                        
                       <!-- end row -->
                       <!-- begin row -->
                        <div class="row">
                        
                           
                       <div class="row">
                            
                               
                       </div>
                       
                           <!-- begin row -->  
                        <div class="row">
                        <div class="col-md-2"></div>
                           <!-- begin col-4 -->
                                <div class="col-md-10">
								 <div class="form-group">
								 <asp:Panel runat="server" ID="PnlTrCivil" Visible="false">
                        <!--CIVIL Salary Details GridView Start-->
                                            <tr id="TrmMess" runat="server" visible="false">
                                                <td colspan="4">
                                                    <asp:GridView ID="GVMess" runat="server" AutoGenerateColumns="false" 
                                                        >
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>SI. No</HeaderTemplate>
                                                                <ItemTemplate><%# Container.DataItemIndex + 1   %></ItemTemplate>
                                                            </asp:TemplateField>
                                                           
                                                         
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>TICKET NO</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblExist" runat="server" Text='<%# Eval("ExisistingCode") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>NAME</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblname" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                             <asp:TemplateField>
                                                                <HeaderTemplate>DEPARTMENT</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbldept" runat="server" Text='<%# Eval("DepartmentNm") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>NO OF DAYS</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorkingDays" runat="server" Text='<%# Eval("WorkedDays") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            
                                                            
                                                         <%--   <asp:TemplateField>
                                                                <HeaderTemplate>NFH</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblNFH" runat="server" Text='<%# Eval("NFh") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>MESS DAYS</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOTHoursNew" runat="server" text='<%# Eval("New_Mess_Days") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>INCENT</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBase" runat="server" Text='<%# Eval("MessAmt") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>AMOUNT</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOthoursAmt" runat="server" Text='<%# Eval("New_Mess_Amt") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            
                                                                                                                    </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <!--CIVIL Salary Details GridView End-->
                       </asp:Panel>
          
          <asp:Panel runat="server" ID="pnlAgent" Visible="false">
                        <!--CIVIL Salary Details GridView Start-->
                                            <tr id="TrAgent" runat="server" visible="false">
                                                <td colspan="4">
                                                    <asp:GridView ID="GVAgent" runat="server" AutoGenerateColumns="false" 
                                                        >
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>SI. No</HeaderTemplate>
                                                                <ItemTemplate><%# Container.DataItemIndex + 1   %></ItemTemplate>
                                                            </asp:TemplateField>
                                                           
                                                         
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>TICKET NO</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblExist" runat="server" Text='<%# Eval("ExisistingCode") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>NAME</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblname" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                             <asp:TemplateField>
                                                                <HeaderTemplate>DEPARTMENT</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbldept" runat="server" Text='<%# Eval("DepartmentNm") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>NO OF DAYS</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorkingDays" runat="server" Text='<%# Eval("New_Agent_Days") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            
                                                            
                                                         <%--   <asp:TemplateField>
                                                                <HeaderTemplate>NFH</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblNFH" runat="server" Text='<%# Eval("NFh") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                            
                                                            
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>INCENT</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBase" runat="server" Text='<%# Eval("Commission") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>AMOUNT</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOthoursAmt" runat="server" Text='<%# Eval("New_Agent_Amt") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            
                                                                                                                    </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <!--CIVIL Salary Details GridView End-->
                       </asp:Panel>
									
									<%--<asp:Button runat="server" id="btnAllSalary" Text="All Salary Details" class="btn btn-success" OnClick="btnAllSalary_Click" Visible="false"/>--%>
									<%--<asp:Button runat="server" id="btnBankSalary" Text="Bank Salary" class="btn btn-success" onclick="btnBankSalary_Click"/>--%>
									<%--<asp:Button runat="server" id="btnOtherBankSalary" Text="Other Bank Salary" class="btn btn-success" onclick="btnOtherBankSalary_Click"/>--%>
									<%--<asp:Button runat="server" id="btnCivilAbstract" Text="Civil Abstract" class="btn btn-success" OnClick="btnCivilAbstract_Click"/>--%>
									<%--<asp:Button runat="server" id="btnLeaveWages" Text="NFH Worked Wages" class="btn btn-success" ValidationGroup="Validate_LeaveField" OnClick="btnLeaveWages_Click" Visible="false"/>--%>
									<%--<asp:Button runat="server" id="BtnDeptManDays" Text="Department Mandays Wages" class="btn btn-success" onclick="BtnDeptManDays_Click" ValidationGroup="Validate_ManField" />--%>
									<%--<asp:Button runat="server" id="btnConfirm" Text="Confirmation" class="btn btn-success" OnClick="btnConfirm_Click" Visible="false"/>--%>
									
						    	 </div>
                               </div>
                              <!-- end col-4 -->
                            
                         </div>
                        <!-- end row --> 
                       
                        </div>
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->



 </ContentTemplate>
                 <Triggers>
                    <asp:PostBackTrigger ControlID="btnSearch" />
                    <asp:PostBackTrigger ControlID="btnExport" />
                    
                 </Triggers>
              </asp:UpdatePanel>
</asp:Content>

