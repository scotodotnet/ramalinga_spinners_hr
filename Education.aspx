<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="Education.aspx.cs" Inherits="Education" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">



<script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>
<%--<script src="assets/js/form-wizards.demo.min.js"></script>--%>
 <script type="text/javascript">
     $(document).ready(function() {
         $('#example').dataTable();
      
     });
	</script>
<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.select2').select2();
                $('#example').dataTable();
            }
        });
    };
</script>

<asp:UpdatePanel ID="UpdatePanel5" runat="server">
 <ContentTemplate>
<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Employee Profile</a></li>
				<li class="active">Education</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Education </h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Education</h4>
                        </div>
                        <div class="panel-body">
                        <!-- begin row -->
                          <div class="row">
                           <!-- begin col-4 -->
                              <div class="col-md-3">
								<div class="form-group">
								  <label>Token No</label>
								  <asp:DropDownList runat="server" ID="ddlTokenNo" class="form-control select2" 
                                        style="width:100%;" AutoPostBack="true" 
                                        onselectedindexchanged="ddlTokenNo_SelectedIndexChanged">
								  </asp:DropDownList>
								</div>
                               </div>
                              <!-- end col-4 -->
                                <!-- begin col-4 -->
                              <div class="col-md-3">
								<div class="form-group">
								  <label>Room No</label>
								  <asp:Label runat="server" ID="lblRoomNo" class="form-control" BackColor="#F3F3F3"></asp:Label>
								</div>
                               </div>
                              <!-- end col-4 -->
                                <!-- begin col-4 -->
                              <div class="col-md-3">
								<div class="form-group">
								  <label>Mobile No</label>
								  <asp:Label runat="server" ID="lblMobNo" class="form-control" BackColor="#F3F3F3"></asp:Label>
								</div>
                               </div>
                              <!-- end col-4 -->
                               <!-- begin col-4 -->
                              <div class="col-md-3">
								<div class="form-group">
								  <label>Qualification</label>
								  <asp:Label runat="server" ID="lblQualification" class="form-control" BackColor="#F3F3F3"></asp:Label>
								</div>
                               </div>
                              <!-- end col-4 -->
                           
                              </div>
                        <!-- end row -->
                       <!-- begin row -->
                          <div class="row">
                          <!-- begin col-4 -->
                                 <div class="col-md-3">
									<div class="form-group">
										<label>Pursuing</label>
									  <asp:DropDownList runat="server" ID="ddlPursuing" class="form-control select2" style="width:100%;">
								       
								     </asp:DropDownList>
									</div>
                                  </div>
                               <!-- end col-4 -->
                               <!-- begin col-3 -->
                             <div class="col-md-3">
								<div class="form-group">
								  <label>University</label>
								  <asp:TextBox runat="server" ID="txtUniv" class="form-control"></asp:TextBox>
								</div>
                               </div>
                              <!-- end col-3 -->
                               <!-- begin col-3 -->
                             <div class="col-md-3">
								<div class="form-group">
								  <label>Course</label>
								  <asp:TextBox runat="server" ID="txtCourse" class="form-control"></asp:TextBox>
								</div>
                               </div>
                              <!-- end col-3 -->
                              <!-- begin col-3 -->
                                 <div class="col-md-3">
									<div class="form-group">
										<label>Department</label>
										<asp:TextBox runat="server" ID="txtDept" class="form-control" ></asp:TextBox>
									</div>
                                  </div>
                               <!-- end col-3 -->
                          </div>
                       <!-- end row -->
                       <!-- begin row -->
                          <div class="row">
                         
                               <!-- begin col-3 -->
                             <div class="col-md-3">
								<div class="form-group">
								  <label>Start Year</label>
								  <asp:TextBox runat="server" ID="txtStartYear" class="form-control"></asp:TextBox>
								</div>
                               </div>
                              <!-- end col-3 -->
                               <!-- begin col-3 -->
                             <div class="col-md-3">
								<div class="form-group">
								  <asp:CheckBox id="chkCompletion" runat="server" AutoPostBack="true" 
                                        oncheckedchanged="chkCompletion_CheckedChanged" /><label>  Completion Year</label>
								  <asp:TextBox runat="server" ID="txtCompletionYear" class="form-control" Enabled="false"></asp:TextBox>
								</div>
                               </div>
                              <!-- end col-3 -->
                              <!-- begin col-3 -->
                                 <div class="col-md-3">
									<div class="form-group">
										<label>Remarks</label>
										<asp:TextBox runat="server" ID="txtRemarks" class="form-control" TextMode="MultiLine" ></asp:TextBox>
									</div>
                                  </div>
                               <!-- end col-3 -->
                               <!-- begin col-3 -->
                                 <div class="col-md-3">
									<div class="form-group">
										<asp:CheckBox id="chkDiscontinue" runat="server" /> <label>Discontinued</label>
										<asp:TextBox runat="server" ID="txtDisContinueRem" class="form-control" TextMode="MultiLine" ></asp:TextBox>
									</div>
                                  </div>
                               <!-- end col-3 -->
                          </div>
                       <!-- end row -->
                         <div class="row">
                         <div class="col-md-4"></div>
                           <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnSave" Text="Save" class="btn btn-success" 
                                         onclick="btnSave_Click"/>
									<asp:Button runat="server" id="btnClear" Text="Clear" class="btn btn-danger" 
                                         onclick="btnClear_Click" />
								 </div>
                               </div>
                              <!-- end col-4 -->
                              <div class="col-md-4"></div>
                         </div>
                        <!-- end row -->
                   
                          <!-- table start -->
					<div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Pursuing</th>
                                               <th>University</th>
                                               <th>StartYear</th>
                                               <th>CompleteYear</th>
                                               <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Container.ItemIndex + 1 %></td>
                                        <td><%# Eval("Pursuing")%></td>
                                        <td><%# Eval("University")%></td>
                                        <td><%# Eval("StartYear")%></td>
                                       <td><%# Eval("CompleteYear")%></td>
                                       
                                        <td>
                                                    <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                        Text="" OnCommand="GridEditEnquiryClick" CommandArgument='<%# Eval("Pursuing")%>' CommandName='<%# Eval("ExistingCode")%>'>
                                                    </asp:LinkButton>
                                                   
                                                </td>
                                       
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </div>
					</div>
					<!-- table End -->
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->
</ContentTemplate>
</asp:UpdatePanel>



</asp:Content>

